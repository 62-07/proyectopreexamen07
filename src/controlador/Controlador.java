package controlador;
import vista.vistaIngreso;
import vista.vistaLista;
import vista.vistaRegistro;
import modelo.Usuarios;
import modelo.dbUsuarios;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener{

    private vistaIngreso vin;
    private vistaLista vlis;
    private vistaRegistro vres;
    private Usuarios user;
    private dbUsuarios dbu;
    private boolean A = false;

    public Controlador(Usuarios user, dbUsuarios dbu, vistaIngreso vin, vistaLista vlis, vistaRegistro vres) {
        this.user = user;
        this.dbu = dbu;
        this.vin = vin;
        this.vlis = vlis;
        this.vres = vres;
        
        vin.btnIngresar.addActionListener(this);
        vin.btnRegistrarse.addActionListener(this);
        vlis.btnCerrarVista.addActionListener(this);
        vres.btnCerrar.addActionListener(this);
        vres.btnGuardar.addActionListener(this);
        vres.btnLimpiar.addActionListener(this);
    }

    private void iniciarIngreso() {
        limpiar();
        vin.setTitle(":: INGRESAR ::");
        vin.setSize(600, 600);
        vin.setLocationRelativeTo(null);
        vin.setVisible(true);
    }
    
    private void iniciarLista() {
        vlis.setTitle(":: LISTA ::");
        vlis.setSize(600, 600);
        vlis.setLocationRelativeTo(null);
        vlis.setVisible(true);
    }
    
    private void iniciarRegistro() {
        vres.setTitle(":: REGISTRAR ::");
        vres.setSize(600, 600);
        vres.setLocationRelativeTo(null);
        vres.setVisible(true);
    }
    
     private void cerrarIngreso() {
        vin.setVisible(false);
    }
    
    private void cerrarLista() {
        vlis.setVisible(false);
    }
    
    private void cerrarRegistro() {
        vres.setVisible(false);
    }

    private void limpiar() {
        vres.txtNewContraseña.setText("");
        vres.txtNewCorreo.setText("");
        vres.txtNewContraseña2.setText("");
        vres.txtNewUsuario.setText("");
        A = (false);
    }

    private void cargarDatos() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Usuarios> lista = new ArrayList<>();
        try {
            lista = dbu.listar();
        } catch (Exception ex) {
        }
        modelo.addColumn("Id");
        modelo.addColumn("usuario");
        modelo.addColumn("correo");
        for (Usuarios producto : lista) {
            modelo.addRow(new Object[]{producto.getIdUsuarios(), producto.getNombre(), producto.getCorreo(), producto.getContraseña()});
        }
        vlis.jtRegistros.setModel(modelo);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       
        if (e.getSource() == vin.btnIngresar) {
            try {
                Usuarios users = new Usuarios();
                if((dbu.isExiste(vin.txtUsuario.getText()))){
                    users = (Usuarios) dbu.buscar(vin.txtUsuario.getText());
                    if(vin.txtContraseña.getText().equals(users.getContraseña())){
                        JOptionPane.showMessageDialog(null, "Usted ha entrado");
                        cerrarIngreso();
                        iniciarLista();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "La contraseña es incorrecta");
                        return;
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "No se encontro el usuario");
                    return;
                }
                
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        
        if(e.getSource() == vin.btnRegistrarse){
            int option = JOptionPane.showConfirmDialog(vin, "¿Desea crear un nuevo usuario?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                vin.dispose();
                cerrarIngreso();
                iniciarRegistro();
            }
        }
        
        if (e.getSource() == vres.btnGuardar) {
            try {
                
                if("".equalsIgnoreCase(vres.txtNewUsuario.getText()))
                {
                    throw new IllegalArgumentException("Escribe un usuario");
                }
                
                if((dbu.isExiste(vres.txtNewUsuario.getText())))
                {
                    JOptionPane.showMessageDialog(vres, "Este usuario ya existe");
                    return;
                }
                
                if("".equalsIgnoreCase(vres.txtNewCorreo.getText()))
                {
                    throw new IllegalArgumentException("Escribe un correo");
                }
                
                if("".equalsIgnoreCase(vres.txtNewContraseña.getText()))
                {
                    throw new IllegalArgumentException("Escribe una contraseña");
                }
                
            
            }catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vres, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }catch (Exception ex2) {
                JOptionPane.showMessageDialog(vres, "Error: " + ex2.getMessage());
                return;
            }

            
            try{
                user.setNombre(vres.txtNewUsuario.getText());
                user.setCorreo(vres.txtNewCorreo.getText());
                user.setContraseña(vres.txtNewContraseña.getText());
                if(vres.txtNewContraseña.getText().equals(vres.txtNewContraseña2.getText()))
                {
                    user.setContraseña(vres.txtNewContraseña2.getText());
                }
                else{
                    JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden");
                    return;
                }
                
                A = true;
                if (A) {
                    dbu.insertar(user);
                    JOptionPane.showMessageDialog(null, "Registro Guardado");
                    limpiar();
                    cargarDatos();
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        
        if(e.getSource() == vres.btnLimpiar){
            limpiar();
        }
        
        if(e.getSource() == vres.btnCerrar){
             int option = JOptionPane.showConfirmDialog(vres, "¿Desea volver a la pantalla de Log In?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                vres.dispose();
                cerrarRegistro();
                iniciarIngreso();
            }
        }
        
        if(e.getSource() == vlis.btnCerrarVista){
             int option = JOptionPane.showConfirmDialog(vlis, "¿Seguro que desea salir?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                vlis.dispose();
                System.exit(0);
                cerrarLista();
            }
        }
    }
        
    public static void main(String[] args) {
        Usuarios U = new Usuarios();
        dbUsuarios UD = new dbUsuarios();
        vistaIngreso vin = new vistaIngreso(new JFrame(), true);
        vistaLista vlis = new vistaLista(new JFrame(), true);
        vistaRegistro vres = new vistaRegistro(new JFrame(), true);
        Controlador contra = new Controlador(U, UD, vin, vlis, vres);
        contra.cargarDatos();
        contra.iniciarIngreso();
    }
}
